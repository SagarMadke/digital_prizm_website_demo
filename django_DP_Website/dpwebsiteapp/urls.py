from django.conf.urls import include, url
from django.contrib import admin
from dpwebsiteapp import views

urlpatterns = [
    url(r'^home-header-form', views.home_header_view, name="home_header_view"),
    url(r'^home-why-us-form', views.home_why_us_view, name="home_why_us_view"),
    url(r'^home-slogan-form', views.home_slogan_view, name="home_slogan_view"),
    url(r'^home-page/', views.home_page_view, name="home_page_view"),
    #url(r'^get_home/', views.get_home_view, name="get_home_view") for json data,
    
    url(r'^sd-header', views.soft_devl_header_view, name="soft_devl_header_view"),
    url(r'^sd-intro', views.soft_dev_intro_view, name="soft_dev_intro_view"),
    url(r'^sd-types', views.soft_dev_types_view, name="soft_dev_types_view"),
    url(r'^software-development/', views.get_sd_view, name="get_sd_view"),
    
    url(r'^big-data-header', views.big_data_header_view, name="big_data_header_view"),
    url(r'^big-data-intro-title', views.big_data_intro_title_view, name="big_data_intro_title_view"),
    url(r'^big-data-intro-description', views.big_data_intro_description_view, name="big_data_intro_description_view"),
    url(r'^big-data-features', views.big_data_features_view, name="big_data_features_view"),
    url(r'^big-data-consulting', views.big_data_consulting_view, name="big_data_consulting_view"),
    url(r'^big-data-types', views.big_data_types_view, name="big_data_types_view"),
    url(r'^big-data/', views.big_data_view, name="big_data_view"),

    url(r'^mobile-development-header', views.mob_dev_header_view, name="mob_dev_header_view"),
    url(r'^mobile-development-intro', views.mob_dev_intro_view, name="mob_dev_intro_view"),
    url(r'^mobile-development-types', views.mob_dev_types_view, name="mob_dev_types_view"),
    url(r'^mobile-development-results', views.mob_dev_results_view, name="mob_dev_results_view"),
    url(r'^mobile-development/', views.mobile_development_view, name="mobile_development_view"),

    url(r'^admin/', admin.site.urls),
]