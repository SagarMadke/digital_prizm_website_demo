from django.db import models

# Defining models for Home Page 
class HomeHeader(models.Model):
	title = models.CharField(max_length=255)
	bgimage = models.ImageField(upload_to = "images/", blank=True)
	description = models.TextField()

	def __str__(self):
		return self.title

class HomeWhyUs(models.Model):
	why_us_title = models.CharField(max_length=255)
	why_us_description = models.TextField()

	def __str__(self):
		return self.why_us_title

class HomeSlogan(models.Model):
	slogan_title = models.TextField()

	def __str__(self):
		return self.slogan_title

#Models for software development page
class SoftwareDevelopmentHeaderModel(models.Model):
	sd_title = models.CharField(max_length=255)
	sd_bgimage = models.ImageField(upload_to = "images/", blank=True)
	sd_description = models.TextField()

	def __str__(self):
		return self.sd_title

class SoftwareDevelopmentIntroModel(models.Model):
	sd_title = models.CharField(max_length=255)
	sd_description = models.TextField()

	def __str__(self):
		return self.sd_title

class SoftwareDevelopmentTypesModel(models.Model):
	sd_title = models.CharField(max_length=255)
	sd_description = models.TextField()

	def __str__(self):
		return self.sd_title

#Models for Big Data web page
class BigDataHeaderModel(models.Model):
	bd_title = models.CharField(max_length=255)
	bd_bgimage = models.ImageField(upload_to = "images/", blank=True)
	bd_description = models.TextField()

	def __str__(self):
		return self.bd_title

class BigDataIntroTitleModel(models.Model):
	bd_title = models.CharField(max_length=255)

	def __str__(self):
		return self.bd_title

class BigDataIntroDescriptionModel(models.Model):
	bd_description = models.TextField()

	def __str__(self):
		return self.bd_description

class BigDataTypesModel(models.Model):
	bd_title = models.CharField(max_length=255)
	bd_description = models.TextField()

	def __str__(self):
		return self.bd_title

class BigDataFeaturesModel(models.Model):
	bd_title = models.CharField(max_length=255)
	bd_description = models.TextField()

	def __str__(self):
		return self.bd_title

class BigDataConsultingModel(models.Model):
	bd_title = models.CharField(max_length=255)
	bd_description = models.TextField()

	def __str__(self):
		return self.bd_title

class BigDataTypesModel(models.Model):
	bd_title = models.CharField(max_length=255)
	bd_description = models.TextField()

	def __str__(self):
		return self.bd_title

# Models for Mobile Developement web page
class MobileDevHeaderModel(models.Model):
	md_title = models.CharField(max_length=255)
	md_bgimage = models.ImageField(upload_to="images/", blank=True)
	md_description = models.TextField()

	def __str__(self):
		return self.md_title

class MobileDevIntroModel(models.Model):
	md_description = models.TextField()

	def __str__(self):
		return self.md_description

class MobileDevTypesModel(models.Model):
	md_title = models.CharField(max_length=255)
	md_description = models.TextField()

	def __str__(self):
		return self.md_title

class MobileDevResultsModel(models.Model):
	md_title = models.CharField(max_length=255)
	md_description = models.TextField()

	def __str__(self):
		return self.md_title

# Model for Contact us web page
class Contact(models.Model):
	pri_email = models.EmailField(max_length=100)
	address1 = models.CharField(max_length=100)
	address2 = models.CharField(max_length=100)
	city = models.CharField(max_length=100)
	pin = models.CharField(max_length=100)
	country = models.CharField(max_length=100)

	def __str__(self):
		return self.country