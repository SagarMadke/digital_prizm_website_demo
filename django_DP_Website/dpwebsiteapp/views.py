from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.core import serializers
import json
# import models of Home, Software Development, Big Data and Mobile Development
from .models import HomeHeader, HomeWhyUs, HomeSlogan
from .models import SoftwareDevelopmentHeaderModel, SoftwareDevelopmentIntroModel, SoftwareDevelopmentTypesModel
from .models import BigDataHeaderModel, BigDataIntroTitleModel, BigDataIntroDescriptionModel, BigDataFeaturesModel, BigDataConsultingModel, BigDataTypesModel
from .models import MobileDevHeaderModel, MobileDevIntroModel, MobileDevTypesModel, MobileDevResultsModel


# Home page views 
def home_page_view(request):
	instance = HomeHeader.objects.all()
	instance2 = HomeWhyUs.objects.all()
	instance3 = HomeSlogan.objects.all()
	return render(request, 'home.html', {'instance':instance, 'instance2':instance2, 'instance3':instance3})

def home_header_view(request):
	if request.method == "POST" :
		title = request.POST ['title']
		bgimage = request.FILES.getlist('image_name')
		description = request.POST ['desc']
		for i in bgimage :
			pic = i
			instance = HomeHeader(title =title, bgimage=pic, description= description,)
			instance.save()
		return redirect('home_page_view')
	return render(request, 'home_header_form.html', {})

def home_why_us_view(request):
	if request.method == "POST" :
		title = request.POST ['title']
		description = request.POST ['desc']
		instance = HomeWhyUs(why_us_title =title, why_us_description= description,)
		instance.save()
		return redirect('home_page_view')
	return render(request, 'home_why_us_form.html', {}) 

def home_slogan_view(request):
	if request.method == "POST" :
		title = request.POST ['slogan']
		instance = HomeSlogan(slogan_title =title,)
		instance.save()
		return redirect('home_page_view')
	return render(request, 'home_slogan_form.html', {})

def get_home_view(request):
	queryset = HomeHeader.objects.all()
	queryset2 = HomeWhyUs.objects.all()
	queryset3 = HomeSlogan.objects.all()
	queryset = serializers.serialize('json', queryset)
	queryset2 = serializers.serialize('json', queryset2)
	queryset3 = serializers.serialize('json', queryset3)
	to_json = {'queryset' : queryset, 'queryset2' : queryset2, 'queryset3' : queryset3}
	return HttpResponse(json.dumps(to_json), content_type = 'application/json')
	
def get_sd_view(request):
	instance = SoftwareDevelopmentHeaderModel.objects.all()
	instance2 = SoftwareDevelopmentIntroModel.objects.all()
	instance3 = SoftwareDevelopmentTypesModel.objects.all()
	return render(request, 'Services_SoftwareDevelopement.html', {'instance':instance, 'instance2':instance2, 'instance3':instance3 })

def soft_devl_header_view(request):
	if request.method == "POST":
		title = request.POST ['title']
		bgimage = request.FILES.getlist('image_name')
		description = request.POST ['desc']
		for i in bgimage :
			pic = i
			instance = SoftwareDevelopmentHeaderModel(sd_title =title, sd_bgimage=pic, sd_description= description)
			instance.save()
		return redirect('get_sd_view')
	return render(request, 'Services_Soft_Devl_header_form.html', {})

def soft_dev_intro_view(request):
	if request.method == "POST" :
		title = request.POST ['title']
		description = request.POST ['desc']
		instance = SoftwareDevelopmentIntroModel(sd_title =title, sd_description= description,)
		instance.save()
		return redirect('get_sd_view')
	return render(request, 'Services_Soft_Devl_Intro_form.html', {})

def soft_dev_types_view(request):
	if request.method == "POST" :
		title = request.POST ['title']
		description = request.POST ['desc']
		instance = SoftwareDevelopmentTypesModel(sd_title =title, sd_description= description,)
		instance.save()
		return redirect('get_sd_view')
	return render(request, 'Services_Soft_Devl_types_form.html', {})



def big_data_view(request):
	instance = BigDataHeaderModel.objects.all()
	instance2 = BigDataIntroTitleModel.objects.all()
	instance3 = BigDataIntroDescriptionModel.objects.all()
	instance4 = BigDataFeaturesModel.objects.all()
	instance5 = BigDataConsultingModel.objects.all()
	instance6 = BigDataTypesModel.objects.all()
	return render (
		request, 
		'Services_Big_Data.html', 
		{'instance':instance, 'instance2':instance2, 'instance3':instance3, 'instance4':instance4, 'instance5':instance5, 'instance6':instance6}
		)

def big_data_header_view(request):
	if request.method == "POST":
		title = request.POST ['title']
		bgimage = request.FILES.getlist('image_name')
		description = request.POST ['desc']
		for i in bgimage :
			pic = i
			instance = BigDataHeaderModel(bd_title =title, bd_bgimage=pic, bd_description= description)
			instance.save()
		return redirect('big_data_view')
	return render(request, 'Services_Big_Data_header_form.html', {})

def big_data_intro_title_view(request):
	if request.method == "POST" :
		title = request.POST ['title']
		instance = BigDataIntroTitleModel(bd_title =title)
		instance.save()
		return redirect('big_data_view')
	return render(request, 'Services_Big_Data_intro_title_form.html', {})

def big_data_intro_description_view(request):
	if request.method == "POST" :
		description = request.POST ['desc']
		instance = BigDataIntroDescriptionModel(bd_description =description)
		instance.save()
		return redirect('big_data_view')
	return render(request, 'Services_Big_Data_intro_description_form.html', {})

def big_data_features_view(request):
	if request.method == "POST":
		title = request.POST ['title']
		description = request.POST ['desc']
		instance = BigDataFeaturesModel(bd_title =title, bd_description= description)
		instance.save()
	return redirect('big_data_view')

def big_data_consulting_view(request):
	if request.method == "POST":
		title = request.POST ['title']
		description = request.POST ['desc']
		instance = BigDataConsultingModel(bd_title =title, bd_description= description)
		instance.save()
	return redirect('big_data_view')

def big_data_types_view(request):
	if request.method == "POST":
		title = request.POST ['title']
		description = request.POST ['desc']
		instance = BigDataTypesModel(bd_title =title, bd_description= description)
		instance.save()
	return redirect('big_data_view')

def mobile_development_view(request):
	instance = MobileDevHeaderModel.objects.all()
	instance2 = MobileDevIntroModel.objects.all()
	instance3 = MobileDevTypesModel.objects.all()
	instance4 = MobileDevResultsModel.objects.all()
	return render(request, 
		'Services_Mobile_Development.html', 
		{'instance':instance, 'instance2':instance2, 'instance3':instance3, 'instance4':instance4}
		)

def mob_dev_header_view(request):
	instance = MobileDevHeaderModel.objects.all()
	return redirect('mobile_development_view')

def mob_dev_intro_view(request):
	instance = MobileDevIntroModel.objects.all()
	return redirect('mobile_development_view')

def mob_dev_types_view(request):
	instance = MobileDevTypesModel.objects.all()
	return redirect('mobile_development_view')

def mob_dev_results_view(request):
	instance = MobileDevResultsModel.objects.all()
	return redirect('mobile_development_view')

def contact_us_view(request):
	instance = Contact.objects.all()
	return render(request, 'contact_us.html', {'instance':instance})