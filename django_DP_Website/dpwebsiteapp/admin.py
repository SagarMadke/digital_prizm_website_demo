from django.contrib import admin
from .models import HomeHeader, HomeWhyUs, HomeSlogan
from .models import SoftwareDevelopmentHeaderModel, SoftwareDevelopmentIntroModel, SoftwareDevelopmentTypesModel
from .models import BigDataHeaderModel, BigDataIntroTitleModel, BigDataIntroDescriptionModel, BigDataFeaturesModel, BigDataConsultingModel, BigDataTypesModel
from .models import MobileDevHeaderModel, MobileDevIntroModel, MobileDevTypesModel, MobileDevResultsModel, Contact

class HomeHeaderModelAdmin(admin.ModelAdmin):
	list_display = ["id", "title", "bgimage"]
	search_fields = ["title"]
	class Meta:
		model = HomeHeader

admin.site.register(HomeHeader, HomeHeaderModelAdmin)

class HomeWhyUsModelAdmin(admin.ModelAdmin):
	list_display = ["id", "why_us_title"]
	search_fields = ["why_us_title"]
	class Meta:
		model = HomeHeader

admin.site.register(HomeWhyUs, HomeWhyUsModelAdmin)

class HomeSloganModelAdmin(admin.ModelAdmin):
	list_display = ["id", "slogan_title"]
	class Meta:
		model = HomeSlogan

admin.site.register(HomeSlogan, HomeSloganModelAdmin)



class SoftwareDevelopmentHeaderModelAdmin(admin.ModelAdmin):
	list_display = ["id", "sd_title"]
	class Meta:
		model = SoftwareDevelopmentHeaderModel
		
admin.site.register(SoftwareDevelopmentHeaderModel, SoftwareDevelopmentHeaderModelAdmin)

class SoftwareDevelopmentIntroModelAdmin(admin.ModelAdmin):
	list_display = ["id", "sd_title"]
	class Meta:
		model = SoftwareDevelopmentIntroModel
		
admin.site.register(SoftwareDevelopmentIntroModel, SoftwareDevelopmentIntroModelAdmin)

class SoftwareDevelopmentTypesModelAdmin(admin.ModelAdmin):
	list_display = ["id", "sd_title"]
	class Meta:
		model = SoftwareDevelopmentTypesModel
		
admin.site.register(SoftwareDevelopmentTypesModel, SoftwareDevelopmentTypesModelAdmin)



class BigDataHeaderModelAdmin(admin.ModelAdmin):
	list_display = ["id", "bd_title"]
	class Meta:
		model = BigDataHeaderModel
		
admin.site.register(BigDataHeaderModel, BigDataHeaderModelAdmin)

class BigDataIntroTitleModelAdmin(admin.ModelAdmin):
	list_display = ["id", "bd_title"]
	class Meta:
		model = BigDataIntroTitleModel
		
admin.site.register(BigDataIntroTitleModel, BigDataIntroTitleModelAdmin)

class BigDataIntroDescriptionModelAdmin(admin.ModelAdmin):
	list_display = ["id", "bd_description"]
	class Meta:
		model = BigDataIntroDescriptionModel
		
admin.site.register(BigDataIntroDescriptionModel, BigDataIntroDescriptionModelAdmin)

class BigDataFeaturesModelAdmin(admin.ModelAdmin):
	list_display = ["id", "bd_title"]
	class Meta:
		model = BigDataFeaturesModel
		
admin.site.register(BigDataFeaturesModel, BigDataFeaturesModelAdmin)

class BigDataConsultingModelAdmin(admin.ModelAdmin):
	list_display = ["id", "bd_title"]
	class Meta:
		model = BigDataConsultingModel
		
admin.site.register(BigDataConsultingModel, BigDataConsultingModelAdmin)

class BigDataTypesModelAdmin(admin.ModelAdmin):
	list_display = ["id", "bd_title"]
	class Meta:
		model = BigDataTypesModel
		
admin.site.register(BigDataTypesModel, BigDataTypesModelAdmin)

class MobileDevHeaderModelAdmin(admin.ModelAdmin):
	list_display= ["id", "md_title"]
	class Meta:
		model = MobileDevHeaderModel

admin.site.register(MobileDevHeaderModel, MobileDevHeaderModelAdmin)

class MobileDevIntroModelAdmin(admin.ModelAdmin):
	list_display = ["id", "md_description"]
	class Meta:
		model = MobileDevIntroModel

admin.site.register(MobileDevIntroModel, MobileDevIntroModelAdmin)

class MobileDevTypesModelAdmin(admin.ModelAdmin):
	list_display = ["id", "md_title"]
	class Meta:
		model = MobileDevTypesModel

admin.site.register(MobileDevTypesModel, MobileDevTypesModelAdmin)

class MobileDevResultsModelAdmin(admin.ModelAdmin):
	list_display = ["id", "md_title"]
	class Meta:
		model = MobileDevResultsModel

admin.site.register(MobileDevResultsModel, MobileDevResultsModelAdmin)

class ContactAdmin(admin.ModelAdmin):
	list_display = ["id", "country"]
	class Meta:
		model = Contact

admin.site.register(Contact, ContactAdmin)