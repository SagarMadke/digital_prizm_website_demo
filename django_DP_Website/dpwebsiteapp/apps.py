from django.apps import AppConfig


class DpwebsiteappConfig(AppConfig):
    name = 'dpwebsiteapp'
